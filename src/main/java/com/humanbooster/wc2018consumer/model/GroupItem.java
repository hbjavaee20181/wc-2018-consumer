package com.humanbooster.wc2018consumer.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class GroupItem implements Serializable {

    private String name;
    private Long winner;
    private Long runnerUp;
    private List<MatchItem> matches;

    public GroupItem() {

    }

    public GroupItem(String name, Long winner, Long runnerUp) {
        this.name = name;
        this.winner = winner;
        this.runnerUp = runnerUp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getWinner() {
        return winner;
    }

    public void setWinner(Long winner) {
        this.winner = winner;
    }

    public Long getRunnerUp() {
        return runnerUp;
    }

    public void setRunnerUp(Long runnerUp) {
        this.runnerUp = runnerUp;
    }

    public List<MatchItem> getMatches() {
        return matches;
    }

    public void setMatches(List<MatchItem> matchs) {
        this.matches = matchs;
    }
}
