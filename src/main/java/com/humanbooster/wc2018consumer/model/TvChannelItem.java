package com.humanbooster.wc2018consumer.model;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

import java.io.Serializable;
import java.util.List;

public class TvChannelItem implements Serializable {

    private Long id;
    private String name;
    private String icon;
    private String country;
    private String iso2;
    private List<String> lang;

    public TvChannelItem() {

    }

    public TvChannelItem(Long id, String name, String icon, String country, String iso2, List<String> lang) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.country = country;
        this.iso2 = iso2;
        this.lang = lang;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getIcon() {
        return icon;
    }

    public String getCountry() {
        return country;
    }

    public String getIso2() {
        return iso2;
    }

    public List<String> getLang() {
        return lang;
    }
}
