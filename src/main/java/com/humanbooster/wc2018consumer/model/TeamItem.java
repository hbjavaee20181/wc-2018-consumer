package com.humanbooster.wc2018consumer.model;

import java.io.Serializable;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class TeamItem implements Serializable {

    private Long id;
    private String name;
    private String fifaCode;
    private String iso2;
    private String flag;
    private String emoji;
    private String emojiString;

    public TeamItem() {

    }

    public TeamItem(Long id, String name, String fifaCode, String iso2, String flag, String emoji, String emojiString) {
        this.id = id;
        this.name = name;
        this.fifaCode = fifaCode;
        this.iso2 = iso2;
        this.flag = flag;
        this.emoji = emoji;
        this.emojiString = emojiString;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFifaCode() {
        return fifaCode;
    }

    public void setFifaCode(String fifaCode) {
        this.fifaCode = fifaCode;
    }

    public String getIso2() {
        return iso2;
    }

    public void setIso2(String iso2) {
        this.iso2 = iso2;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getEmoji() {
        return emoji;
    }

    public void setEmoji(String emoji) {
        this.emoji = emoji;
    }

    public String getEmojiString() {
        return emojiString;
    }

    public void setEmojiString(String emojiString) {
        this.emojiString = emojiString;
    }
}
