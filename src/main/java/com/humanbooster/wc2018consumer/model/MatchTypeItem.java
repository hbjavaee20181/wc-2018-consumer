package com.humanbooster.wc2018consumer.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public enum MatchTypeItem {
    @SerializedName("group")
    GROUP,

    @SerializedName("winner")
    WINNER,

    @SerializedName("loser")
    LOSER,

    @SerializedName("qualified")
    QUALIFIED
}