package com.humanbooster.wc2018consumer.model;

import java.io.Serializable;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class StadiumItem implements Serializable {

    private Long id;
    private String name;
    private String city;
    private Double lat;
    private Double lng;
    private String image;

    public StadiumItem() {

    }

    public StadiumItem(Long id, String name, String city, Double lat, Double lng, String image) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.lat = lat;
        this.lng = lng;
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
