package com.humanbooster.wc2018consumer.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class WordCupJsonItem implements Serializable {

    private List<StadiumItem> stadiums;
    private List<TvChannelItem> tvchannels;
    private List<TeamItem> teams;
    private Map<String, GroupItem> groups;
    private Map<String, KnockoutItem> knockout;

    public WordCupJsonItem() {

    }

    public List<StadiumItem> getStadiums() {
        return stadiums;
    }

    public void setStadiums(List<StadiumItem> stadiums) {
        this.stadiums = stadiums;
    }

    public List<TvChannelItem> getTvchannels() {
        return tvchannels;
    }

    public void setTvchannels(List<TvChannelItem> tvchannels) {
        this.tvchannels = tvchannels;
    }

    public List<TeamItem> getTeams() {
        return teams;
    }

    public void setTeams(List<TeamItem> teams) {
        this.teams = teams;
    }

    public Map<String, GroupItem> getGroups() {
        return groups;
    }

    public void setGroups(Map<String, GroupItem> groups) {
        this.groups = groups;
    }


    public Map<String, KnockoutItem> getKnockout() {
        return knockout;
    }

    public void setKnockout(Map<String, KnockoutItem> knockout) {
        this.knockout = knockout;
    }
}
