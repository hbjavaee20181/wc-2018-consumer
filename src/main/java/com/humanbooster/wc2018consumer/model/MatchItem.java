package com.humanbooster.wc2018consumer.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class MatchItem implements Serializable {

    private Long name;

    private MatchTypeItem type;

    @SerializedName("home_team")
    private Long homeTeam;

    @SerializedName("away_team")
    private Long awayTeam;

    @SerializedName("home_result")
    private Long homeResult;

    @SerializedName("away_result")
    private Long awayResult;

    private Date date;

    private Long stadium;

    private List<Long> channels;

    private Boolean finished;

    private Integer matchday;

    public MatchItem(Long name, MatchTypeItem type, Long homeTeam, Long awayTeam, Long homeResult, Long awayResult, Date date, Long stadium, List<Long> channels, Boolean finished, Integer matchday) {
        this.name = name;
        this.type = type;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeResult = homeResult;
        this.awayResult = awayResult;
        this.date = date;
        this.stadium = stadium;
        this.channels = channels;
        this.finished = finished;
        this.matchday = matchday;
    }

    public MatchItem() {

    }

    public Long getName() {
        return name;
    }

    public void setName(Long name) {
        this.name = name;
    }

    public MatchTypeItem getType() {
        return type;
    }

    public void setType(MatchTypeItem type) {
        this.type = type;
    }

    public Long getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Long homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Long getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Long awayTeam) {
        this.awayTeam = awayTeam;
    }

    public Long getHomeResult() {
        return homeResult;
    }

    public void setHomeResult(Long homeResult) {
        this.homeResult = homeResult;
    }

    public Long getAwayResult() {
        return awayResult;
    }

    public void setAwayResult(Long awayResult) {
        this.awayResult = awayResult;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getStadium() {
        return stadium;
    }

    public void setStadium(Long stadium) {
        this.stadium = stadium;
    }

    public List<Long> getChannels() {
        return channels;
    }

    public void setChannels(List<Long> channels) {
        this.channels = channels;
    }

    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public Integer getMatchday() {
        return matchday;
    }

    public void setMatchday(Integer matchday) {
        this.matchday = matchday;
    }
}
