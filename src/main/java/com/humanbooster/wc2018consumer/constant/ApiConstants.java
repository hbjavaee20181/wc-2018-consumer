package com.humanbooster.wc2018consumer.constant;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public final class ApiConstants {

    private ApiConstants() {

    }

    public static final String API_URL = "https://raw.githubusercontent.com/lsv/fifa-worldcup-2018/master/data.json";
}
